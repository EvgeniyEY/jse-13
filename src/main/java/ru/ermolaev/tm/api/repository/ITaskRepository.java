package ru.ermolaev.tm.api.repository;

import ru.ermolaev.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    List<Task> findAll();

    void remove(Task task);

    void clear();

    Task findById(String id);

    Task findByIndex(Integer index);

    Task findByName(String name);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task removeByName(String name);

}
