package ru.ermolaev.tm.api.service;

import ru.ermolaev.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void createProject(String name);

    void createProject(String name, String description);

    void addProject(Project project);

    List<Project> showAllProjects();

    void removeProject(Project project);

    void removeAllProjects();

    Project findProjectById(String id);

    Project findProjectByIndex(Integer index);

    Project findProjectByName(String name);

    Project updateProjectById(String id, String name, String description);

    Project updateProjectByIndex(Integer index, String name, String description);

    Project removeProjectById(String id);

    Project removeProjectByIndex(Integer index);

    Project removeProjectByName(String name);

}
