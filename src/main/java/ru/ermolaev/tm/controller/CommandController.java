package ru.ermolaev.tm.controller;

import ru.ermolaev.tm.api.service.ICommandService;
import ru.ermolaev.tm.api.controller.ICommandController;
import ru.ermolaev.tm.model.Command;
import ru.ermolaev.tm.util.NumberUtil;

public class CommandController implements ICommandController{

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) System.out.println(command);
    }

    public void showCommands() {
        final String[] commands = commandService.getCommands();
        for (final String command: commands) System.out.println(command);
    }

    public void showArguments() {
        final String[] arguments = commandService.getArgs();
        for (final String argument: arguments) System.out.println(argument);
    }

    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Evgeniy Ermolaev");
        System.out.println("E-MAIL: ermolaev.evgeniy.96@yandex.ru");
    }

    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.10");
    }

    public void showInfo() {
        System.out.println("Available processors (cores): " + Runtime.getRuntime().availableProcessors());
        System.out.println("Free memory: " + NumberUtil.formatBytes(Runtime.getRuntime().freeMemory()));
        System.out.println("Maximum memory: " + (Runtime.getRuntime().maxMemory() == Long.MAX_VALUE ? "no limit" : NumberUtil.formatBytes(Runtime.getRuntime().maxMemory())));
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(Runtime.getRuntime().totalMemory()));
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()));
    }

    public void exit() {
        System.exit(0);
    }

}
