package ru.ermolaev.tm.error;

public class ArgumentDoesNotExistException extends RuntimeException {

    public ArgumentDoesNotExistException() {
        super("Error! This argument does not exist.");
    }

    public ArgumentDoesNotExistException(final String argument) {
        super("Error! This argument [" + argument + "] does not exist.");
    }

}
