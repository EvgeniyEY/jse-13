package ru.ermolaev.tm.error;

public class CommandDoesNotExistException extends RuntimeException {

    public CommandDoesNotExistException() {
        super("Error! This command does not exist.");
    }

    public CommandDoesNotExistException(final String command) {
        super("Error! This command [" + command + "] does not exist.");
    }

}
