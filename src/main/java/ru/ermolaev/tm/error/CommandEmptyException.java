package ru.ermolaev.tm.error;

public class CommandEmptyException extends RuntimeException {

    public CommandEmptyException() {
        super("Error! Command is empty.");
    }

}
