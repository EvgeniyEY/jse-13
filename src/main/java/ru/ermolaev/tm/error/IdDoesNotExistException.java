package ru.ermolaev.tm.error;

public class IdDoesNotExistException extends RuntimeException {

    public IdDoesNotExistException() {
        super("Error! This ID does not exist.");
    }

    public IdDoesNotExistException(final String id) {
        super("Error! This ID [" + id + "] does not exist.");
    }

}
