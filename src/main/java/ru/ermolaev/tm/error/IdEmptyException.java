package ru.ermolaev.tm.error;

public class IdEmptyException extends RuntimeException {

    public IdEmptyException() {
        super("Error! ID is empty.");
    }

}
