package ru.ermolaev.tm.error;

public class IndexIncorrectException extends RuntimeException {

    public IndexIncorrectException() {
        super("Error! Index is incorrect.");
    }

    public IndexIncorrectException(final String value) {
        super("Error! This value [" + value + "] is not number.");
    }

}
