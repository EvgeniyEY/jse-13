package ru.ermolaev.tm.error;

public class NameDoesNotExistException extends RuntimeException {

    public NameDoesNotExistException() {
        super("Error! This name does not exist.");
    }

    public NameDoesNotExistException(final String name) {
        super("Error! This name [" + name + "] does not exist.");
    }

}
