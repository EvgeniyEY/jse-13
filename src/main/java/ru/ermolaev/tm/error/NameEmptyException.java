package ru.ermolaev.tm.error;

public class NameEmptyException extends RuntimeException {

    public NameEmptyException() {
        super("Error! Name is empty.");
    }

}
